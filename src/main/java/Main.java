import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {
        FactorialServiceImpl service = new FactorialServiceImpl();
        FactorialServiceHandler handler = new FactorialServiceHandler(service, 10);
        FactorialService proxy = (FactorialService) Proxy.newProxyInstance(service.getClass().getClassLoader(), FactorialServiceImpl.class.getInterfaces(), handler);
        getFactorialFromTo(proxy, 0, 10);
        getFactorialFromTo(proxy, 0, 15);
    }

    private static void getFactorialFromTo(FactorialService service, int i, int j) {
        for (; i < j; ++i) {
            try {
                System.out.println("Factorial for " + i + " is " + service.getFactorial(i));
            } catch (IllegalArgumentException exception) {
                System.out.println(exception.getMessage());
            }
        }
    }

}
