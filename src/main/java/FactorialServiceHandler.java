import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

public class FactorialServiceHandler implements InvocationHandler {

    private final FactorialServiceImpl service;

    private final int capacity;

    private final Map<Integer, Integer> values;

    public FactorialServiceHandler(FactorialServiceImpl service, int capacity) {
        this.service = service;
        this.capacity = capacity;
        this.values = new LinkedHashMap<>() {
            @Override
            protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
                return size() > capacity;
            }
        };
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("getFactorial")) {
            Integer key = (Integer)args[0];
            if (this.values.containsKey(key)) {
                System.out.println("From cache");
                return this.values.get(key);
            } else {
                Integer value = (Integer)method.invoke(service, args);
                this.values.put(key, value);
                return  value;
            }
        }
        return method.invoke(service, args);
    }

}
