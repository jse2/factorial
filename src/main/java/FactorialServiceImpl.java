import java.util.stream.IntStream;

public class FactorialServiceImpl implements FactorialService {

    @Override
    public int getFactorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Факториал сущесвует для неотрицательного целого числа");
        }
        return IntStream.rangeClosed(1, n).reduce(1, (x, y) -> x * y);
    }

}
