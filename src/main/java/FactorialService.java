public interface FactorialService {
    int getFactorial(int n);
}
